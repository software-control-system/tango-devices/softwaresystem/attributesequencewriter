#include <yat4tango/ExceptionHelper.h>
#include "AttributesHelper.h"

size_t NUMBER_RETRIES=3;
const unsigned int TIME_BETWEEN_RETRIES_IN_MS = 2000;

//=============================================================================
// _MSEC_SLEEP MACRO
//============================================================================= 
#if defined(WIN32)
# define MSEC_SLEEP(T) ::Sleep(T)
#else
# define MSEC_SLEEP(T) ::usleep(1000 * T);
#endif

/*********************************************************************
* AttributesHelper::AttributesHelper
**********************************************************************/ 
AttributesHelper::AttributesHelper(Tango::DeviceImpl* dev, string read_attr, string write_attr)
  : Tango::LogAdapter(dev), 
    m_proxy_write(0),
    m_proxy_read(0),
    m_attr_read(read_attr),
    m_attr_write(write_attr),
    m_proxy_state(Tango::FAULT),
    m_proxy_status("attributes proxy state unknown")
{
	if (m_attr_read != "0")
  {
		m_proxy_read = new Tango::AttributeProxy(m_attr_read);

    if (m_proxy_read == 0)
    {
      yat::OSStream oss;
      oss << "Could not create AttributeProxy for reading" << std::endl;
      ERROR_STREAM << oss.str() << std::endl;
      m_proxy_status = oss.str();
      m_proxy_state = Tango::FAULT;

      THROW_DEVFAILED("DEVICE_ERROR", oss.str().c_str(), "AttributesHelper::AttributesHelper");
    }
  }

  m_proxy_write =  new Tango::AttributeProxy(m_attr_write);

  if (m_proxy_write == 0)
  {
    yat::OSStream oss;
    oss << "Could not create AttributeProxy for writing" << std::endl;
    ERROR_STREAM << oss.str() << std::endl;
    m_proxy_status = oss.str();
    m_proxy_state = Tango::FAULT;

    THROW_DEVFAILED("DEVICE_ERROR", oss.str().c_str(), "AttributesHelper::AttributesHelper");
  }

    m_proxy_status = "Attribute proxy ok";
    m_proxy_state = Tango::ON;
}

/*********************************************************************
* AttributesHelper::~AttributesHelper
**********************************************************************/ 
AttributesHelper::~AttributesHelper()
{
	if (m_proxy_read)
	{
		delete m_proxy_read;
		m_proxy_read = 0;
	}

	if (m_proxy_write)
	{
		delete m_proxy_write;
		m_proxy_write = 0;
	}
}
/*********************************************************************
* AttributesHelper::state_read_attribute
**********************************************************************/   
Tango::DevState AttributesHelper::state_read_attribute()
{
	INFO_STREAM << "AttributesHelper::state_read_attribute() entering " << endl; 
	Tango::DevState state= Tango::FAULT;
	size_t nr = 0;
  std::stringstream msg;

	while ( nr < NUMBER_RETRIES) 
	 { 
		try
		{
		INFO_STREAM << "AttributesHelper::state_write_attribute() before _proxy_write->state() " << endl; 
		state = m_proxy_read->state();
		break;
		}
		catch (Tango::DevFailed &e)
		{
		// increment retries count 
			nr++;

			FATAL_STREAM << e << endl; 
			msg << "Cannot get state for " << m_attr_write << ":" << endl;

			// read the exception stack
			for (unsigned int i=0; i<e.errors.length(); i++)
			{
				msg << e.errors[i].reason.in() << endl;
				msg << e.errors[i].desc.in() << endl;
				msg << e.errors[i].origin.in() << endl;
			}
			msg << "retrying... [remaining attempts " << NUMBER_RETRIES - nr << "]" << std::endl;
			msg << endl;
			MSEC_SLEEP(TIME_BETWEEN_RETRIES_IN_MS);
		} // end catch
  	} // end while 

  	// if not successful after NUMBER_RETRIES then throw
		if (NUMBER_RETRIES == nr)
			{
      m_proxy_status = msg.str();
      m_proxy_state = Tango::FAULT;
			THROW_DEVFAILED("COMMUNICATION_ERROR",
                    msg.str().c_str(),
                    "AttributesHelper::state_read_attribute()");
 
			}
	return state;

}
/*********************************************************************
* AttributesHelper::read_attribute
**********************************************************************/   
double AttributesHelper::read_attribute()
{
	INFO_STREAM << "AttributesHelper::read_attribute() entering " << endl; 
	if (m_proxy_read)
	{
    Tango::DeviceAttribute dev_attr;

   size_t nr = 0;
   std::stringstream msg;

 while ( nr < NUMBER_RETRIES) 
  { 
		try
		{
			dev_attr = m_proxy_read->read();
			break;
		}
		catch (Tango::DevFailed &e)
		{
			msg << "Cannot read " << m_attr_read << ":" << endl;
			// increment retries count 
			nr++;
			FATAL_STREAM << e <<endl; 
		
			// read the exception stack
			for (unsigned int i = 0; i < e.errors.length(); i++)
			{
				msg << e.errors[i].reason.in() << endl;
				msg << e.errors[i].desc.in() << endl;
				msg << e.errors[i].origin.in() << endl;
			}
	 		msg << "retrying... [remaining attempts " << NUMBER_RETRIES - nr << "]" << std::endl;
			msg << endl;
			MSEC_SLEEP(TIME_BETWEEN_RETRIES_IN_MS);
		} // end catch

	} // end while	

	// if not successful after NUMBER_RETRIES then throw
		if (NUMBER_RETRIES == nr)
			{
      m_proxy_status = msg.str();
      m_proxy_state = Tango::FAULT;
			THROW_DEVFAILED("COMMUNICATION_ERROR",
                    msg.str().c_str(),
                    "AttributesHelper::read_attribute()");
			}

// Now check attribute type
// 
		double value;
		short s_val;
		long l_val;
		int data_type;

		data_type = m_proxy_read->get_config().data_type;

		if (m_proxy_read->get_config().data_format != Tango::SCALAR)
		{	   
			ERROR_STREAM << "Cannot read device attribute that is not a scalar" << endl;

      m_proxy_status = "Cannot read device attribute that is not a scalar";
      m_proxy_state = Tango::FAULT;

      THROW_DEVFAILED("DEVICE_ERROR", "Cannot read device attribute that is not a scalar", "AttributesHelper::read_attribute");
		}

		switch (data_type)
		{
		case 2://short
			dev_attr >> s_val;
			value = (double) s_val;
			break;
		case 3://long
			dev_attr >> l_val;
			value = (double) l_val;
			break;
		case 5://double
			dev_attr >> value;
			break; 
		default:
			ERROR_STREAM << "Cannot read device attribute that is not short, long, or double" << endl; 

      m_proxy_status = "Cannot read device attribute that is not short, long, or double";
      m_proxy_state = Tango::FAULT;

      THROW_DEVFAILED("DEVICE_ERROR", "Cannot read device attribute that is not short, long, or double", "AttributesHelper::read_attribute");
			break; 
		} // end switch

		INFO_STREAM << "\t\t ****read  " << m_attr_read << ": " << value << endl;

		return value;
	}

	return 0;
}
/*********************************************************************
* AttributesHelper::state_write_attribute
**********************************************************************/   
Tango::DevState AttributesHelper::state_write_attribute()
{
	INFO_STREAM << "AttributesHelper::state_write_attribute() entering " << endl; 
	Tango::DevState state= Tango::FAULT;
	size_t nr = 0;
  std::stringstream msg;

	while ( nr < NUMBER_RETRIES) 
	 { 
		try
		{
		INFO_STREAM << "AttributesHelper::state_write_attribute() before _proxy_write->state() " << endl; 
		state = m_proxy_write->state();
		break;
		}
		catch (Tango::DevFailed &e)
		{
		// increment retries count 
			nr++;

			FATAL_STREAM << e << endl; 
			msg << "Cannot get state for " << m_attr_write << ":" << endl;

			// read the exception stack
			for (unsigned int i=0; i<e.errors.length(); i++)
			{
				msg << e.errors[i].reason.in() << endl;
				msg << e.errors[i].desc.in() << endl;
				msg << e.errors[i].origin.in() << endl;
			}
			msg << "retrying... [remaining attempts " << NUMBER_RETRIES - nr << "]" << std::endl;
			msg << endl;
			MSEC_SLEEP(TIME_BETWEEN_RETRIES_IN_MS);
		} // end catch
  	} // end while 

  	// if not successful after NUMBER_RETRIES then throw
		if (NUMBER_RETRIES == nr)
			{
      m_proxy_status = msg.str();
      m_proxy_state = Tango::FAULT;
 			THROW_DEVFAILED("COMMUNICATION_ERROR",
                    msg.str().c_str(),
                    "AttributesHelper::state_write_attribute()");
 
			}

	return state;
}
/*********************************************************************
* AttributesHelper::write_attribute
**********************************************************************/ 
void AttributesHelper::write_attribute(double val)
{ 
	INFO_STREAM << "AttributesHelper::write_attribute() entering,  value= " << val << endl; 
 	size_t nr = 0;
  std::stringstream msg;

	if (m_proxy_write)
	{
		int data_type;

		INFO_STREAM << "\t\t ****write on " << m_attr_write << ": " << val << endl;

		if (m_proxy_write->get_config().data_format != Tango::SCALAR)
		{
			ERROR_STREAM << "Cannot write device attribute that is not a scalar" << endl;

      m_proxy_status = "Cannot write device attribute that is a scalar";
      m_proxy_state = Tango::FAULT;
      THROW_DEVFAILED("DEVICE_ERROR", "Cannot write device attribute that is not a scalar", "AttributesHelper::write_attribute");
		}

		try
		{
			data_type = m_proxy_write->get_config().data_type;
		}
		catch (Tango::DevFailed &e)
		{
			std::stringstream msg;
			msg << "Cannot get data type of  " << m_attr_write << ":" << endl;

			// read the exception stack
			for (unsigned int i=0; i<e.errors.length(); i++)
			{
				msg << e.errors[i].reason.in() << endl;
				msg << e.errors[i].desc.in() << endl;
				msg << e.errors[i].origin.in() << endl;
			}
			msg << ends;

      m_proxy_status = msg.str();
      m_proxy_state = Tango::FAULT;
			throw;
		}

    Tango::DeviceAttribute dev_attr;

		switch (data_type)
		{
		case 2://short
			dev_attr << (short)val;
			break;
		case 3://long
			dev_attr << (long)val;
			break;
		case 5://double
			dev_attr <<val;
			break;
		default:
			ERROR_STREAM << "Cannot write device attribute that is not short, long, or double" << endl; 
      m_proxy_status = "Cannot write device attribute that is not short, long, or double";
      m_proxy_state = Tango::FAULT;

      THROW_DEVFAILED("DEVICE_ERROR", "Cannot write device attribute that is not short, long, or double", "AttributesHelper::write_attribute");
			break; 
		}


	// Now let's do the real writing
  while ( nr < NUMBER_RETRIES) 
  { 
		try
		{
			m_proxy_write->write(dev_attr);
			break;
		}
		catch (Tango::DevFailed &e)
		{
		  // increment retries count 
			nr++;
			FATAL_STREAM << e <<endl; 
			msg << "Cannot write on " << m_attr_write << ":" << endl;

			// read the exception stack
			for (unsigned int i=0; i<e.errors.length(); i++)
			{
				msg << e.errors[i].reason.in() << endl;
				msg << e.errors[i].desc.in() << endl;
				msg << e.errors[i].origin.in() << endl;
			}

			msg << "retrying... [remaining attempts " << NUMBER_RETRIES - nr << "]" << std::endl;
			msg << endl;

			MSEC_SLEEP(TIME_BETWEEN_RETRIES_IN_MS);
		} // end catch
 	} // end while 

  	// if not successful after NUMBER_RETRIES then throw
		if (NUMBER_RETRIES == nr)
			{
      m_proxy_status = msg.str();
      m_proxy_state = Tango::FAULT;
			THROW_DEVFAILED("COMMUNICATION_ERROR",
                    msg.str().c_str(),
                    "AttributesHelper::write_attribute()");
 
			}
	}
}

