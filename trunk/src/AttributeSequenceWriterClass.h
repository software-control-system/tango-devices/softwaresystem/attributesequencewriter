//=============================================================================
//
// file :         AttributeSequenceWriterClass.h
//
// description :  Include for the AttributeSequenceWriterClass root class.
//                This class is the singleton class for
//                the AttributeSequenceWriter device class.
//                It contains all properties and methods which the 
//                AttributeSequenceWriter requires only once e.g. the commands.
//			
// project :      TANGO Device Server
//
// $Author: pascal_verdier $
//
// $Revision: 14110 $
// $Date: 2010-02-10 08:47:17 +0100 (Wed, 10 Feb 2010) $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source$
// $Log$
// Revision 3.8  2009/04/07 10:53:56  pascal_verdier
// Tango-7 release.
// SVN tags added
//
// Revision 3.7  2008/04/07 12:01:57  pascal_verdier
// CVS put property modified.
//
// Revision 3.6  2007/10/23 14:04:30  pascal_verdier
// Spelling mistakes correction
//
// Revision 3.5  2007/09/14 14:36:08  pascal_verdier
// Add an ifdef WIN32 for dll generation
//
// Revision 3.4  2005/09/08 08:45:23  pascal_verdier
// For Pogo-4.4.0 and above.
//
// Revision 3.3  2005/03/02 14:06:15  pascal_verdier
// namespace is different than class name.
//
// Revision 3.2  2004/11/08 11:33:16  pascal_verdier
// if device property not found in database, it takes class property value if exists.
//
// Revision 3.1  2004/09/06 09:27:05  pascal_verdier
// Modified for Tango 5 compatibility.
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#ifndef _ATTRIBUTESEQUENCEWRITERCLASS_H
#define _ATTRIBUTESEQUENCEWRITERCLASS_H

#include <tango.h>
#include <AttributeSequenceWriter.h>


namespace AttributeSequenceWriter_ns
{//=====================================
//	Define classes for attributes
//=====================================
class waitingTimesAttrib: public Tango::SpectrumAttr
{
public:
	waitingTimesAttrib():SpectrumAttr("waitingTimes", Tango::DEV_DOUBLE, Tango::READ, 1000000) {};
	~waitingTimesAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<AttributeSequenceWriter *>(dev))->read_waitingTimes(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<AttributeSequenceWriter *>(dev))->is_waitingTimes_allowed(ty);}
};

class sequenceValuesAttrib: public Tango::SpectrumAttr
{
public:
	sequenceValuesAttrib():SpectrumAttr("sequenceValues", Tango::DEV_DOUBLE, Tango::READ, 1000000) {};
	~sequenceValuesAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<AttributeSequenceWriter *>(dev))->read_sequenceValues(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<AttributeSequenceWriter *>(dev))->is_sequenceValues_allowed(ty);}
};

class sequenceNameAttrib: public Tango::Attr
{
public:
	sequenceNameAttrib():Attr("sequenceName", Tango::DEV_STRING, Tango::READ_WRITE) {};
	~sequenceNameAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<AttributeSequenceWriter *>(dev))->read_sequenceName(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<AttributeSequenceWriter *>(dev))->write_sequenceName(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<AttributeSequenceWriter *>(dev))->is_sequenceName_allowed(ty);}
};

class currentIterationAttrib: public Tango::Attr
{
public:
	currentIterationAttrib():Attr("currentIteration", Tango::DEV_DOUBLE, Tango::READ) {};
	~currentIterationAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<AttributeSequenceWriter *>(dev))->read_currentIteration(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<AttributeSequenceWriter *>(dev))->is_currentIteration_allowed(ty);}
};

class currentIterationProgressionAttrib: public Tango::Attr
{
public:
	currentIterationProgressionAttrib():Attr("currentIterationProgression", Tango::DEV_SHORT, Tango::READ) {};
	~currentIterationProgressionAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<AttributeSequenceWriter *>(dev))->read_currentIterationProgression(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<AttributeSequenceWriter *>(dev))->is_currentIterationProgression_allowed(ty);}
};

class totalProgressionAttrib: public Tango::Attr
{
public:
	totalProgressionAttrib():Attr("totalProgression", Tango::DEV_SHORT, Tango::READ) {};
	~totalProgressionAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<AttributeSequenceWriter *>(dev))->read_totalProgression(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<AttributeSequenceWriter *>(dev))->is_totalProgression_allowed(ty);}
};

class currentSequenceValueAttrib: public Tango::Attr
{
public:
	currentSequenceValueAttrib():Attr("currentSequenceValue", Tango::DEV_DOUBLE, Tango::READ) {};
	~currentSequenceValueAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<AttributeSequenceWriter *>(dev))->read_currentSequenceValue(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<AttributeSequenceWriter *>(dev))->is_currentSequenceValue_allowed(ty);}
};

//=========================================
//	Define classes for commands
//=========================================
class LoadWaitingTimesCmd : public Tango::Command
{
public:
	LoadWaitingTimesCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	LoadWaitingTimesCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~LoadWaitingTimesCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<AttributeSequenceWriter *>(dev))->is_LoadWaitingTimes_allowed(any);}
};



class LoadSequenceValuesCmd : public Tango::Command
{
public:
	LoadSequenceValuesCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	LoadSequenceValuesCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~LoadSequenceValuesCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<AttributeSequenceWriter *>(dev))->is_LoadSequenceValues_allowed(any);}
};



class ResumeCmd : public Tango::Command
{
public:
	ResumeCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	ResumeCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~ResumeCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<AttributeSequenceWriter *>(dev))->is_Resume_allowed(any);}
};



class PauseCmd : public Tango::Command
{
public:
	PauseCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	PauseCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~PauseCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<AttributeSequenceWriter *>(dev))->is_Pause_allowed(any);}
};



class StopCmd : public Tango::Command
{
public:
	StopCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	StopCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~StopCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<AttributeSequenceWriter *>(dev))->is_Stop_allowed(any);}
};



class StartCmd : public Tango::Command
{
public:
	StartCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	StartCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~StartCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<AttributeSequenceWriter *>(dev))->is_Start_allowed(any);}
};



//
// The AttributeSequenceWriterClass singleton definition
//

class
#ifdef _TG_WINDOWS_
	__declspec(dllexport)
#endif
	AttributeSequenceWriterClass : public Tango::DeviceClass
{
public:
//	properties member data

//	add your own data members here
//------------------------------------

public:
	Tango::DbData	cl_prop;
	Tango::DbData	cl_def_prop;
	Tango::DbData	dev_def_prop;

//	Method prototypes
	static AttributeSequenceWriterClass *init(const char *);
	static AttributeSequenceWriterClass *instance();
	~AttributeSequenceWriterClass();
	Tango::DbDatum	get_class_property(string &);
	Tango::DbDatum	get_default_device_property(string &);
	Tango::DbDatum	get_default_class_property(string &);
	
protected:
	AttributeSequenceWriterClass(string &);
	static AttributeSequenceWriterClass *_instance;
	void command_factory();
	void get_class_property();
	void attribute_factory(vector<Tango::Attr *> &);
	void write_class_property();
	void set_default_property();
	string get_cvstag();
	string get_cvsroot();

private:
	void device_factory(const Tango::DevVarStringArray *);
};


}	//	namespace AttributeSequenceWriter_ns

#endif // _ATTRIBUTESEQUENCEWRITERCLASS_H
