
#ifndef _ATTRIBUTES_HELPER_
#define _ATTRIBUTES_HELPER_

#include <tango.h>

//=============================================================================
// class AttributesHelper
//=============================================================================
class AttributesHelper : public Tango::LogAdapter
{
public:
  
  AttributesHelper(Tango::DeviceImpl* dev, string read_attr, string write_attr); 
  
  virtual ~AttributesHelper(); 
  
  double read_attribute();
  
  void write_attribute(double val);

  Tango::DevState state_read_attribute();
  
  Tango::DevState state_write_attribute();

  // Returns proxy state
  Tango::DevState get_proxy_state()
  {
    return m_proxy_state;
  }

  // Returns proxy status
  std::string get_proxy_status()
  {
    return m_proxy_status;
  }

private:
  
  Tango::AttributeProxy* m_proxy_write;
  
  Tango::AttributeProxy* m_proxy_read;
  
  string m_attr_read;
  
  string m_attr_write;
  
  // Attribute proxy state
  Tango::DevState m_proxy_state;

  // Attribute proxy status
  std::string m_proxy_status;
  
};


#endif //_ATTRIBUTES_HELPER_
