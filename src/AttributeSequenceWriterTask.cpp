// ============================================================================
//
// = CONTEXT
//		TANGO Project - DeviceTask example
//
// = File
//		AttributeSequenceWriterTask.cpp
//
// = AUTHOR
//		N.Leclercq - SOLEIL & JC.Pret - APSIDE
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <math.h>
#include <yat/threading/Mutex.h>
#include "AttributeSequenceWriterTask.h"

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
#define kSTART_PERIODIC_MSG (yat::FIRST_USER_MSG + 1000)
#define kSTOP_PERIODIC_MSG  (yat::FIRST_USER_MSG + 1001)
#define kSTART_MSG          (yat::FIRST_USER_MSG + 1002)
#define kRESUME_MSG         (yat::FIRST_USER_MSG + 1004)
#define kSEQ_VALUES_CHANGE_MSG      (yat::FIRST_USER_MSG + 1005)
#define kWAITING_TIMES_CHANGE_MSG   (yat::FIRST_USER_MSG + 1006)

namespace AttributeSequenceWriter_ns
{

// ============================================================================
// AttributeSequenceWriterTask::AttributeSequenceWriterTask
// ============================================================================
AttributeSequenceWriterTask::AttributeSequenceWriterTask (const Config & cfg)
  : yat4tango::DeviceTask(cfg.host_device),
    m_cfg(cfg),
    m_task_state(TANGO_STATE_FOR_INIT),
    m_task_status("Device is initializing")
{
  //- configure optional msg handling
  this->enable_timeout_msg(false);
  this->enable_periodic_msg(false);
  this->set_periodic_msg_period(cfg.task_activity_period_ms);

  // initialize sequence booleans
  m_paused = false;
  m_stopped = false;
  m_is_started = false;
  m_tmo_occured = false;

  m_attr_proxy = 0;

  m_sem = 0;
}

// ============================================================================
// AttributeSequenceWriterTask::~AttributeSequenceWriterTask
// ============================================================================
AttributeSequenceWriterTask::~AttributeSequenceWriterTask ()
{
  //- noop
}

// ============================================================================
// AttributeSequenceWriterTask::process_message
// ============================================================================
void AttributeSequenceWriterTask::process_message (yat::Message& msg)
  throw (Tango::DevFailed)
{
  //- handle msg
  switch (msg.type())
  {
    //- THREAD_INIT ----------------------
    case yat::TASK_INIT:
      {
	      DEBUG_STREAM << "AttributeSequenceWriterTask::handle_message::THREAD_INIT::thread is starting up" << std::endl;
        try
        {
          this->init_i();
        }
        catch (...)
        {
          throw;
        }
      }
	    break;

	  //- THREAD_EXIT ----------------------
	  case yat::TASK_EXIT:
	    {
			  DEBUG_STREAM << "AttributeSequenceWriterTask::handle_message::THREAD_EXIT::thread is quitting" << std::endl;
        try
        {
          this->fini_i();
        }
        catch (...)
        {
          // ignore any error...
        }
      }
		  break;

	  //- THREAD_PERIODIC ------------------
	  case yat::TASK_PERIODIC:
	    {
        //DEBUG_STREAM << "AttributeSequenceWriterTask::handle_message::THREAD_PERIODIC" << std::endl;
        this->periodic_job_i();
      }
	    break;

	  //- THREAD_TIMEOUT -------------------
	  case yat::TASK_TIMEOUT:
	    {
        //- not used in this example
      }
      break;

    //- kSTART_PERIODIC_MSG --------------
    case kSTART_PERIODIC_MSG:
      {
    	  DEBUG_STREAM << "AttributeSequenceWriterTask::handle_message::kSTART_PERIODIC_MSG" << std::endl;
        this->enable_periodic_msg(true);
      }
      break;

    //- kSTOP_PERIODIC_MSG ---------------
    case kSTOP_PERIODIC_MSG:
      {
    	  DEBUG_STREAM << "AttributeSequenceWriterTask::handle_message::kSTOP_PERIODIC_MSG" << std::endl;
        this->enable_periodic_msg(false);
      }
      break;

    //- kSTART_MSG --------------
    case kSTART_MSG:
      {
    	  DEBUG_STREAM << "AttributeSequenceWriterTask::handle_message::kSTART_MSG" << std::endl;
        this->start_i();
      }
      break;

    //- kSEQ_VALUES_CHANGE_MSG --------------
    case kSEQ_VALUES_CHANGE_MSG:
      {
    	  DEBUG_STREAM << "AttributeSequenceWriterTask::handle_message::kSEQ_VALUES_CHANGE_MSG" << std::endl;
        this->m_cfg.seq_values = msg.get_data<yat::Buffer<double> >();
      }
      break;

    //- kWAITING_TIMES_CHANGE_MSG --------------
    case kWAITING_TIMES_CHANGE_MSG:
      {
    	  DEBUG_STREAM << "AttributeSequenceWriterTask::handle_message::kWAITING_TIMES_CHANGE_MSG" << std::endl;
        this->m_cfg.wait_times = msg.get_data<yat::Buffer<double> >();
      }
      break;

    //- UNHANDLED MSG --------------------
		default:
		  DEBUG_STREAM << "AttributeSequenceWriterTask::handle_message::unhandled msg type received" << std::endl;
			break;
  }
}

// ============================================================================
// AttributeSequenceWriterTask::init_i
// ============================================================================
void AttributeSequenceWriterTask::init_i ()
    throw (Tango::DevFailed)
{
  DEBUG_STREAM << "AttributeSequenceWriterTask::init_i <-" << std::endl;

  try
  {
    m_sem = new yat::Semaphore(0);

    //if there is no attribute to read, don't check
    if (m_cfg.attr_read == "0")
      m_check_read_value = false;
    else
      m_check_read_value = true;

#if defined (_PS_AVAILABLE_)
    m_attr_proxy = new AttributesHelper(m_cfg.host_device, m_cfg.attr_read, m_cfg.attr_write);
#endif

    m_task_state = TANGO_STATE_FOR_READY;
    set_sequence_status( "The sequence generation is ready to run.");
  }
  catch (Tango::DevFailed & e)
  {
    m_task_state = Tango::FAULT;
    set_sequence_status("Initialization failed!");
    ERROR_STREAM << "AttributeSequenceWriterTask::init_i::initialization failed" << std::endl;
    ERROR_STREAM << e << std::endl;
    RETHROW_DEVFAILED(e,
                      "DEVICE_ERROR",
                      "AttributeSequenceWriterTask::init_i::initialization failed",
                      "AttributeSequenceWriterTask::init_i");
  }
  catch (...)
  {
    m_task_state = Tango::FAULT;
    set_sequence_status( "Initialization failed!");
    ERROR_STREAM << "AttributeSequenceWriterTask::init_i::initialization failed - unknown exception caught" << std::endl;
    THROW_DEVFAILED("UNKNOWN_ERROR",
                    "AttributeSequenceWriterTask::init_i::initialization failed - unknown exception caught",
                    "AttributeSequenceWriterTask::init_i");
  }

  DEBUG_STREAM << "AttributeSequenceWriterTask::init_i ->" << std::endl;
}

// ============================================================================
// AttributeSequenceWriterTask::exit
// ============================================================================
void AttributeSequenceWriterTask::exit ()
    throw (Tango::DevFailed)
{
  DEBUG_STREAM << "AttributeSequenceWriterTask::exit <-" << std::endl;

  this->stop();
  this->yat4tango::DeviceTask::exit();
}

// ============================================================================
// AttributeSequenceWriterTask::fini_i
// ============================================================================
void AttributeSequenceWriterTask::fini_i ()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "AttributeSequenceWriterTask::fini_i <-" << std::endl;

  //- release memory
	if(m_attr_proxy)
		delete m_attr_proxy;
  m_attr_proxy = 0;

  if(m_sem)
    delete m_sem;
  m_sem = 0;

  DEBUG_STREAM << "AttributeSequenceWriterTask::fini_i ->" << std::endl;
}

// ============================================================================
// AttributeSequenceWriterTask::seq_values_changed
// ============================================================================
void AttributeSequenceWriterTask::seq_values_changed (const yat::Buffer<double> & data)
{
  this->post(kSEQ_VALUES_CHANGE_MSG, data, 1000);
}

// ============================================================================
// AttributeSequenceWriterTask::waiting_times_changed
// ============================================================================
void AttributeSequenceWriterTask::waiting_times_changed (const yat::Buffer<double> & data)
{
  this->post(kWAITING_TIMES_CHANGE_MSG, data, 1000);
}

// ============================================================================
// AttributeSequenceWriterTask::start_periodic_activity
// ============================================================================
void AttributeSequenceWriterTask::start_periodic_activity (size_t tmo_ms)
    throw (Tango::DevFailed)
{
  try
  {
    //- synchronous approach: "post then wait for the message to be handled"
    if (tmo_ms)
      this->wait_msg_handled(kSTART_PERIODIC_MSG, tmo_ms);
    //- asynchronous approach: "post the data then return immediately"
    else
      this->post(kSTART_PERIODIC_MSG);
  }
  catch (const Tango::DevFailed&)
  {
    //- an exception could be thrown if the task msgQ is full (high water mark reached)
    //- in the synchronous case we could also caught an exception thrown by the code
    //- handling the message
    throw;
  }
}

// ============================================================================
// AttributeSequenceWriterTask::stop_periodic_activity
// ============================================================================
void AttributeSequenceWriterTask::stop_periodic_activity (size_t tmo_ms)
    throw (Tango::DevFailed)
{
  try
  {
    //- synchronous approach: "post then wait for the message to be handled"
    if (tmo_ms)
      this->wait_msg_handled(kSTOP_PERIODIC_MSG, tmo_ms);
    //- asynchronous approach: "post the data then returm immediately"
    else
      this->post(kSTOP_PERIODIC_MSG);
  }
  catch (const Tango::DevFailed&)
  {
    //- an exception could be thrown if the task msgQ is full (high water mark reached)
    //- in the synchronous case we could also caught an exception thrown by the code
    //- handling the message
    throw;
  }
}

// ============================================================================
// AttributeSequenceWriterTask::get_current_sequence_value
// ============================================================================
void AttributeSequenceWriterTask::get_data_return_values(DataReturnValues & dest)
{
    dest = m_data;
}

// ============================================================================
// AttributeSequenceWriterTask::start
// ============================================================================
void AttributeSequenceWriterTask::start (size_t tmo_ms)
    throw (Tango::DevFailed)
{
  if (0 == this->m_cfg.seq_values.capacity())
  {
    ERROR_STREAM << "AttributeSequenceWriterTask::start::empty sequence!" << std::endl;
    THROW_DEVFAILED("UNKNOWN_ERROR",
                    "AttributeSequenceWriterTask::start::empty sequence!",
                    "AttributeSequenceWriterTask::start");
  }

  if (0 == this->m_cfg.wait_times.capacity())
  {
    ERROR_STREAM << "AttributeSequenceWriterTask::start::empty sequence!" << std::endl;
    THROW_DEVFAILED("UNKNOWN_ERROR",
                    "AttributeSequenceWriterTask::start::empty sequence!",
                    "AttributeSequenceWriterTask::start");
  }

  try
  {
    //- synchronous approach: "post then wait for the message to be handled"
    if (tmo_ms)
      this->wait_msg_handled(kSTART_MSG, tmo_ms);
    //- asynchronous approach: "post the data then return immediately"
    else
      this->post(kSTART_MSG);
  }
  catch (const Tango::DevFailed&)
  {
    //- an exception could be thrown if the task msgQ is full (high water mark reached)
    //- in the synchronous case we could also caught an exception thrown by the code
    //- handling the message
    throw;
  }
}

// ============================================================================
// AttributeSequenceWriterTask::stop
// ============================================================================
void AttributeSequenceWriterTask::stop (size_t tmo_ms)
    throw (Tango::DevFailed)
{
  INFO_STREAM << "Stop received! Current sequence interrupted." << endl;

  //- stop the sequence
  this->m_stopped = true;

  //- seq. might be in pause!
  if (m_paused)
  {
    m_paused = false;
    m_sem->post();
  }

  m_task_state = TANGO_STATE_FOR_READY;
  set_sequence_status( "The sequence is stopped - sequence generation is ready to run.");
}

// ============================================================================
// AttributeSequenceWriterTask::pause
// ============================================================================
void AttributeSequenceWriterTask::pause (size_t tmo_ms)
    throw (Tango::DevFailed)
{
   INFO_STREAM << "Pause received! Current sequence interrupted." << endl;
   m_paused = true;
   m_task_state = TANGO_STATE_FOR_SEQ_PAUSE;
   set_sequence_status( "The sequence is paused.");
}

// ============================================================================
// AttributeSequenceWriterTask::resume
// ============================================================================
void AttributeSequenceWriterTask::resume (size_t tmo_ms)
    throw (Tango::DevFailed)
{
 	  INFO_STREAM << "Resume received! Current sequence continued." << std::endl;

    if (m_paused)
    {
       m_paused = false;
       m_sem->post();
    }

	  m_task_state = Tango::RUNNING;
	  set_sequence_status("The sequence is running.");
}

// ============================================================================
// AttributeSequenceWriterTask::periodic_job_i
// ============================================================================
void AttributeSequenceWriterTask::periodic_job_i ()
  throw (Tango::DevFailed)
{
  //-
}


// ============================================================================
// AttributeSequenceWriterTask::start_i
// ============================================================================
void AttributeSequenceWriterTask::start_i ()
  throw (Tango::DevFailed)
{
  //- start the sequence
  this->enable_periodic_msg(true);
  this->set_periodic_msg_period((size_t)(m_cfg.polling_period * 1000));
  m_task_state = Tango::RUNNING;
  set_sequence_status("The sequence is running");

  bool condition;
  m_data.current_iteration = 1;
  m_data.error_occured = false;

  // time for checking timeout
  double current_tmo_time = 0;
  int total_size = m_cfg.iterations * m_cfg.seq_size;
  int nb_current_val = 0;

  // minumum time of the all sequence
  double total_time_per_seq = 0;

  for (unsigned int i = 0; i < m_cfg.seq_size; i++)
	  total_time_per_seq = total_time_per_seq + ceil(m_cfg.wait_times[i]);

  // time realized until now for the current sequence
  double current_time = 0;

  this->m_is_started =true;

	// write the sequence
	do
	{
		INFO_STREAM << " " << endl;
		INFO_STREAM << "-------------starting iteration number " << m_data.current_iteration << "------------" << endl;
		current_time = 0;

    for (unsigned int seq = 0; seq < m_cfg.seq_size; seq++)
		{
			//check if stopped by user
			if (this->m_stopped)
			{
				break;
			}

      INFO_STREAM << "\t --writing value number " << seq <<" of the sequence--" << endl;

			double write = m_cfg.seq_values[seq];
			m_data.current_sequence_value = write;

			current_tmo_time = 0;

			try
			{
				//check state of the underlying device
#if defined (_PS_AVAILABLE_)
				if (m_attr_proxy->state_write_attribute() == Tango::FAULT)
        {
					m_task_state = Tango::FAULT;
					  set_sequence_status("The sequence cannot start because the underlying device is in FAULT");
					this->m_data.error_occured = true;
					this->m_stopped = true;
				}
#endif
			}
			catch(Tango::DevFailed &)
			{
				m_task_state = Tango::FAULT;
				set_sequence_status("The sequence cannot start because the underlying device is not responding as expected :" + m_attr_proxy -> get_proxy_status() );
				this->m_data.error_occured = true;
				this->m_stopped = true;
			}

			try
			{
				// write sequence value
#if defined (_PS_AVAILABLE_)
				m_attr_proxy->write_attribute(write);
#endif
			}
			catch (Tango::DevFailed &)
			{
				m_task_state = Tango::FAULT;
				set_sequence_status("The sequence cannot start because the underlying device is not responding as expected :" + m_attr_proxy -> get_proxy_status() );
				this->m_data.error_occured = true;
				this->m_stopped = true;
			}

			// poll read value
			if (m_check_read_value)
			{
				double read = 0.0;

				do
				{
					//check if stopped by user
					if (this->m_stopped)
					{
						break;
					}

          try
          {
            //check state of the underlying device
#if defined (_PS_AVAILABLE_)
            if (m_attr_proxy->state_read_attribute() == Tango::FAULT)
            {
              m_task_state = Tango::FAULT;
                set_sequence_status("The sequence cannot start because the underlying device is in FAULT");
              this->m_data.error_occured = true;
              this->m_stopped = true;
            }
#endif
          }
          catch (Tango::DevFailed &)
          {
				    m_task_state = Tango::FAULT;
					set_sequence_status("The sequence cannot start because the underlying device is not responding as expected :" + m_attr_proxy -> get_proxy_status() );
            this->m_data.error_occured = true;
            this->m_stopped = true;
          }

					try
					{
#if defined (_PS_AVAILABLE_)
						read = m_attr_proxy->read_attribute();
#endif
					}
					catch (Tango::DevFailed &)
					{
				    m_task_state = Tango::FAULT;
					set_sequence_status("The sequence cannot start because the underlying device is not responding as expected :" + m_attr_proxy -> get_proxy_status() );
						this->m_data.error_occured = true;
						this->m_stopped = true;
					}

					//check if stopped by user
					if (this->m_stopped)
					{
						break;
					}

					//if paused by user, wait for resume
          if (m_paused)
					{
						INFO_STREAM << "\t\t #####pause##### " <<endl;
						m_sem->wait();
						INFO_STREAM << "\t\t #####resume#####" <<endl;
					  //check if stopped by user or exit or quit or ...
					  if (this->m_stopped)
						  break;
					}

					INFO_STREAM << "\t\t sleep for polling period: " << m_cfg.polling_period << " s" << endl;

					unsigned long secs = (unsigned long)this->m_cfg.polling_period;
					unsigned long nsecs = (unsigned long)((this->m_cfg.polling_period - (double)secs) * (double)1000000000.);

          yat::ThreadingUtilities::sleep(secs, nsecs);

					// check timeout
					current_tmo_time = current_tmo_time + this->m_cfg.polling_period;
					INFO_STREAM << "\t\t checking read value since : " << current_tmo_time << " s" << endl;

					std::stringstream msg;
					msg << "The sequence generation is running."<< endl;
					msg << "Waiting for the desired value to be reached: " << endl;
					msg << "- The reached value is: " << read << endl;
					msg << "- Waiting for desired value since : " << current_tmo_time << " s" <<endl;
					msg << ends;
					  set_sequence_status( msg.str());

					if (current_tmo_time >= this->m_cfg.timeout)
					{
						this->m_stopped = true;
						this->m_tmo_occured = true;
						this->m_data.error_occured = true;
						ERROR_STREAM << "\t\t #####timeout#####" << endl;
            m_task_state = Tango::FAULT;

            std::stringstream msg;
						msg << "The generation has been stopped by timeout (" << m_cfg.timeout << " s)" << ":" << endl;
						msg << "- The reached value is: " << read << endl;
						msg << "- The desired value was: " << write << endl;
						msg << "- The minimum delta was: " << m_cfg.delta << endl;
						msg << ends;

  				  set_sequence_status(msg.str());
					}
				} while ((fabs(read-write) > m_cfg.delta) && (!m_tmo_occured));
			} // end of checking value

			// check if stop requested by user
			if (m_stopped || this->m_tmo_occured)
			{
				INFO_STREAM << "\t\t #####stop#####" << endl;
				break;
			}

			// if pause requested by user, wait for resume
			if (m_paused)
			{
				INFO_STREAM << "\t\t #####pause##### " << endl;
				m_sem->wait();
				INFO_STREAM << "\t\t #####resume#####" << endl;
			}

			// apply the waiting time
			std::stringstream msg;
			msg << "The sequence generation is running." << endl;
			msg << "- Applying waiting of : " <<m_cfg.wait_times[seq] << " s" <<endl;
			msg << ends;
			  set_sequence_status(msg.str());

			INFO_STREAM << "\t\t sleep for waiting time: " << m_cfg.wait_times[seq] << " s" << endl;
			double secs = m_cfg.wait_times[seq];
			unsigned long nsecs = (unsigned long)((m_cfg.wait_times[seq] - secs) * (double)1000000000.);

      // make several small sleeps to be able to pause and stop
      yat::ThreadingUtilities::sleep(0, nsecs);

			for (unsigned int i = 0; i < secs; i++)
			{
				INFO_STREAM << "\t\t sleep for 1s" << endl;

        //sleep for 1 sec
        yat::ThreadingUtilities::sleep(1);

				// calculate current time ignoring nsecs
				current_time = current_time + 1.0;
        m_data.progression_current_iteration = (short)((current_time /total_time_per_seq) * 100);

        // if not infinite loop -> calculate total progression
				if (total_size)
				{
					//current time for all iterations until now
					double current_total_time =  total_time_per_seq * (m_data.current_iteration - 1) + current_time;

          //total time for all iterations
					double total_time =  total_time_per_seq * m_cfg.iterations;
          m_data.total_progression = (short)((current_total_time / total_time) * 100);
				}

				// check if stopped by user
				if (m_stopped || m_tmo_occured)
				{
					INFO_STREAM << "\t\t #####stop#####" << endl;
					break;
				}

				// if paused by user, wait for resume
				if(m_paused)
				{
					INFO_STREAM << "\t\t #####pause##### " << endl;
					m_sem->wait();
					INFO_STREAM << "\t\t #####resume#####" << endl;
				}
			}

		} //for sequence

		m_data.current_iteration++;

		// check if using almost infinite loop
		if (m_cfg.iterations == 0)
			condition = !m_stopped ;
		else
			condition = (m_data.current_iteration <= m_cfg.iterations) && !m_stopped;

	} while (condition);

	m_data.current_iteration--;

	if (!this->m_data.error_occured)
	{
		m_task_state = TANGO_STATE_FOR_READY;
		  set_sequence_status("The sequence generation is ready to run");
	}

	INFO_STREAM << "=======================end of sequence=======================" << endl;

	this->m_is_started = false;

  if (this->m_stopped)
    this->m_stopped = false;

  if (this->m_tmo_occured)
    this->m_tmo_occured = false;
}

// ============================================================================
// AttributeSequenceWriterTask::get_sequence_state
// ============================================================================
Tango::DevState AttributeSequenceWriterTask::get_sequence_state ()
{
  return m_task_state;
}

    // ============================================================================
// AttributeSequenceWriterTask::get_sequence_status
// ============================================================================
    std::string AttributeSequenceWriterTask::get_sequence_status ()
    {
        yat::AutoMutex <yat::Mutex> lock (com_status_mutex);

        return m_task_status;
    }

// ============================================================================
// AttributeSequenceWriterTask::set_sequence_status
// ============================================================================
    void AttributeSequenceWriterTask::set_sequence_status (std::string new_status)
    {
        yat::AutoMutex <yat::Mutex> lock (com_status_mutex);

        m_task_status=new_status;
    }
} // namespace
