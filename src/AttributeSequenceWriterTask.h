// ============================================================================
//
// = CONTEXT
//		DeviceTask example
//
// = File
//		AttributeSequenceWriterTask.h
//
// = AUTHOR
//    N.Leclercq - SOLEIL & JC.Pret - APSIDE
//
// ============================================================================

#ifndef _MY_DEVICE_TASK_H_
#define _MY_DEVICE_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>

#include <yat/any/GenericContainer.h>
#include <yat/memory/DataBuffer.h>
#include <yat4tango/DeviceTask.h>
#include <yat/threading/Semaphore.h>

#include "AttributesHelper.h"

namespace AttributeSequenceWriter_ns
{

// ============================================================================
// DEVICE STATE DEFINITION
// ============================================================================
#if defined (_RECOMMANDED_STATE_MGNT_)
//- Device is ready and waiting for sequence generation (start command):
#define TANGO_STATE_FOR_READY Tango::STANDBY

//- Sequence has been paused by user ; device is waiting for sequence continuation (resume command):
#define TANGO_STATE_FOR_SEQ_PAUSE Tango::DISABLE

//- Device is initializing:
#define TANGO_STATE_FOR_INIT Tango::INIT

#else
//- Device is ready and waiting for sequence generation (start command) :
#define TANGO_STATE_FOR_READY Tango::INIT

//- Sequence has been paused by user ; device is waiting for sequence continuation (resume command):
#define TANGO_STATE_FOR_SEQ_PAUSE Tango::STANDBY

//- Device is initializing:
#define TANGO_STATE_FOR_INIT Tango::OFF //not used state, only for compatibility

#endif

// ============================================================================
// class: AttributeSequenceWriterTask
// ============================================================================
class AttributeSequenceWriterTask : public yat4tango::DeviceTask
{
public:

    //- the task's configuration data struct
    typedef struct Config
    {
        //-default ctor
        Config ()
            : task_activity_period_ms(1000),
              host_device(0),
              iterations(0),
              delta(0),
              timeout(0),
              polling_period(0),
              seq_size(0),
              attr_read(""),
              attr_write("")
        {}
        //- copy ctor
        Config (const Config & src)
        {
            *this = src;
        }
        //- operator=
        const Config & operator= (const Config & src)
        {
            if (&src == this)
                return *this;
            task_activity_period_ms = src.task_activity_period_ms;
            host_device = src.host_device;
            iterations = src.iterations;
            delta = src.delta;
            timeout = src.timeout;
            polling_period = src.polling_period;
            seq_size = src.seq_size;
            attr_read = src.attr_read;
            attr_write = src.attr_write;
            seq_values = src.seq_values;
            wait_times = src.wait_times;
            return *this;
        }
        //- period of task's periodic activity in msecs
        size_t task_activity_period_ms;
        //- the Tango device hosting this task
        Tango::DeviceImpl * host_device;
        //- iterations
        unsigned long iterations;
        //- delta between read and set
        double delta;
        //- timeout
        double timeout;
        //- polling period for read (in s)
        double polling_period;
        //- sequence size
        unsigned long seq_size;
        //- attribute to read
        string attr_read;
        //- attribute to write
        string attr_write;
        //- sequence values
        yat::Buffer<double> seq_values;
        //- waiting times
        yat::Buffer<double> wait_times;
    } Config;

    //- the data return values for monitoring
    typedef struct DataReturnValues
    {
        //-default ctor
        DataReturnValues ()
            : current_sequence_value(0),
              total_progression(0),
              progression_current_iteration(0),
              current_iteration(0),
              error_occured(false)
        {}
        //- copy ctor
        DataReturnValues (const DataReturnValues & src)
        {
            *this = src;
        }
        //- operator=
        const DataReturnValues & operator= (const DataReturnValues & src)
        {
            if (&src == this)
                return *this;
            current_sequence_value = src.current_sequence_value;
            total_progression = src.total_progression;
            progression_current_iteration = src.progression_current_iteration;
            current_iteration = src.current_iteration;
            error_occured = src.error_occured;
            return *this;
        }

        //- current sequence value
        double current_sequence_value;

        //- total progression
        short total_progression;

        //- progression_current_iteration
        short progression_current_iteration;

        //- current_iteration
        double current_iteration;

        //- error flag
        bool error_occured;
    } DataReturnValues;

    //- ctor
    AttributeSequenceWriterTask (const Config & cfg);

    //- dtor
    virtual ~AttributeSequenceWriterTask ();

    //- specialization of the exit behaviour
    virtual void exit ()
    throw (Tango::DevFailed);

    //- starts task's periodic activity (a <tmo_ms> of 0 means asynchronous exec)
    void start_periodic_activity (size_t tmo_ms = 0)
    throw (Tango::DevFailed);

    //- stops task's periodic activity (a <tmo_ms> of 0 means asynchronous exec)
    void stop_periodic_activity (size_t tmo_ms = 0)
    throw (Tango::DevFailed);

    //- start the writing of the sequence
    void start(size_t tmo_ms = 0)
    throw (Tango::DevFailed);

    //- stop the writing of the sequence
    void stop(size_t tmo_ms = 0)
    throw (Tango::DevFailed);

    //- pause the writing of the sequence
    void pause(size_t tmo_ms = 0)
    throw (Tango::DevFailed);

    //- resume the writing of the sequence
    void resume(size_t tmo_ms = 0)
    throw (Tango::DevFailed);

    //- get the current data return values
    void get_data_return_values(DataReturnValues & dest);

    //- get sequence state
    Tango::DevState get_sequence_state();

    //- get/ser sequence status
    std::string get_sequence_status();
    void set_sequence_status (std::string new_status);

    //- seq. values changed
    void seq_values_changed (const yat::Buffer<double> & data);

    //- waiting times changed
    void waiting_times_changed (const yat::Buffer<double> & data);

protected:
    //- process_message (implements yat4tango::DeviceTask pure virtual method)
    virtual void process_message (yat::Message& msg)
    throw (Tango::DevFailed);

    //- initialization
    virtual void init_i ()
    throw (Tango::DevFailed);

    //- cleanup
    virtual void fini_i ()
    throw (Tango::DevFailed);

private:
    //- encapsulates the periodic activity
    void periodic_job_i ()
    throw (Tango::DevFailed);

    //- encapsulate the start function
    void start_i()
    throw (Tango::DevFailed);

    //- the task's configuration
    Config m_cfg;

    //- the data return values
    DataReturnValues m_data;

    //- pause variable
    bool m_paused;

    //- stop variable
    bool m_stopped;

    //- start variable
    bool m_is_started;

    //- read value variable
    bool m_check_read_value;

    //- timeout occured
    bool m_tmo_occured;

    //- Attribute proxy
    AttributesHelper * m_attr_proxy;

    //- pause/resume management
    yat::Semaphore * m_sem;

    // Task state
    Tango::DevState m_task_state;

    // Task status
    std::string m_task_status;

    yat::Mutex com_status_mutex;
};

} // namespace GroupManager_ns

#endif // _MY_DEVICE_TASK_H_
